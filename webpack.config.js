var path = require('path');

module.exports = {
  entry: [
    path.resolve(__dirname, 'web-src/index.js')
  ],
  output: {
    path: path.resolve(__dirname, "web/static/build"),
    publicPath: "/static/",
    
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 'css-loader' ],
      }
    ]
  }
}
