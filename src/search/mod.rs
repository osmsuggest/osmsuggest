use crate::server::Search;
use serde::Serialize;
use std::error::Error;
use std::fmt;
use tantivy::collector::TopDocs;
use tantivy::query::QueryParser;
use tantivy::{DocAddress, Index, Score};
use tantivy::Query::*;

///
/// A place result
///
#[derive(Debug, Serialize)]
pub struct Place {
    name: String,
    display_name: String,
}

#[derive(Debug)]
pub struct SearchError;

impl Error for SearchError {}

impl fmt::Display for SearchError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Could not process the search")
    }
}

pub fn get_matching_places(index: &Index, search: &Search) -> Result<Vec<Place>, SearchError> {
    let name = index.schema().get_field("name").unwrap();
    let display_name = index.schema().get_field("display_name").unwrap();
    let reader = index.reader().unwrap();
    let searcher = reader.searcher();
    let mut query_parser = QueryParser::for_index(&index, vec![name, display_name]);
    query_parser.set_conjunction_by_default();
    let query = match query_parser.parse_query(&search.q) {
        Ok(q) => q,
        Err(e) => {
            return Err(SearchError {});
        }
    };
    let collector = TopDocs::with_limit(10);
    let top_docs: Vec<(Score, DocAddress)> = searcher.search(&query, &(collector)).unwrap();
    let mut places: Vec<Place> = Vec::with_capacity(10);
    for (_score, doc_address) in top_docs {
        let doc = searcher.doc(doc_address).unwrap();
        let p = Place {
            name: doc.get_first(name).unwrap().text().unwrap().to_string(),
            display_name: doc
                .get_first(display_name)
                .unwrap()
                .text()
                .unwrap()
                .to_string(),
        };
        places.push(p);
    }

    Ok(places)
}
