use crate::config::*;
use crate::index::get_index;
use crate::search::get_matching_places;
use actix_web::error::ErrorBadRequest;
use actix_web::{fs, http, server, App, Error, FromRequest, HttpRequest, HttpResponse, Result};
use std::path::PathBuf;
use tantivy::Index;

struct AppState {
    index: Index,
    public_path: String,
}

// Parameters of the request query
#[derive(Debug)]
pub struct Search {
    pub q: String,
}

impl<AppState> FromRequest<AppState> for Search {
    type Config = ();
    type Result = Result<Search, Error>;

    fn from_request(req: &HttpRequest<AppState>, _cfg: &Self::Config) -> Self::Result {
        let query = req.query();

        if !query.contains_key("q") {
            return Err(ErrorBadRequest("q is not defined".to_string()));
        }

        Ok(Search {
            q: query.get("q").expect("q is empty").to_string(),
        })
    }
}

//
// render home page
//
fn home(request: &HttpRequest<AppState>) -> Result<fs::NamedFile> {
    let mut path = PathBuf::new();
    path.push(&request.state().public_path);
    path.push("index.html");
    Ok(fs::NamedFile::open(path).expect("file not found"))
}

//
// process a request query and return the results
//
fn search(request: &HttpRequest<AppState>) -> HttpResponse {
    let index = &request.state().index;
    

    let search = match Search::extract(request) {
        Ok(s) => s,
        Err(e) => return HttpResponse::BadRequest().body(e.to_string()),
    };

    println!("searcher is {:?}", search);

    let places = match get_matching_places(&index, &search) {
        Err(e) => return HttpResponse::BadRequest().body(e.to_string()),
        Ok(p) => p
    };

    HttpResponse::Ok().json(places)
}

// launch the server
pub fn serve(config: Config) {
    let config_cloned = config.clone();

    server::new(move || {
        let index = get_index(&config_cloned.index);

        App::with_state(AppState {
            index: index,
            public_path: config_cloned.server.public_path.clone(),
        })
        .resource("/", |r| r.f(home))
        .resource("/api/search", |r| r.method(http::Method::GET).f(search))
        .handler(
            "/static",
            fs::StaticFiles::new(format!(
                "{}/static",
                config_cloned.server.public_path.clone()
            ))
            .unwrap()
            .show_files_listing(),
        )
    })
    .bind(&config.server.bind_path)
    .unwrap()
    .run();
}
