extern crate unidecode;

use tantivy::tokenizer::*;
use tantivy::Index;

/*
struct UnidecodeTokenizer;

struct UnidecodeTokenizerStream<TailTokenStream>
where
    TailTokenSTream: TokenStrea
{
    tail: TailTokenStream,
}

impl TokenFilter<TailTokenStream> for UnidecodeTokenizer<TailTokenStream>
where
    TailTokenStream: TokenStream,
{
    type ResultTokenStream: UnidecodeTokenizerStream<TailTokenStream>;

    fn transform(&self, token_stream: TailTokenStream) -> Self::ResultTokenStream {

    }
}*/

pub fn add_custom_tokenizer(index: &Index) -> Result<(), String> {
    let name_tokenizer = NgramTokenizer::new(2, 3, false).filter(LowerCaser);

    index
        .tokenizers()
        .register("name_tokenizer", name_tokenizer);

    Ok(())
}

/*
#[cfg(test)]
mod tests {
    use tantivy::tokenizer::*;

    #[test]
    fn test_tokens() {
        let tokenizer = NgramTokenizer::new(2, 3, true).filter(LowerCaser);
        let mut stream = tokenizer.token_stream("Bachi bouzouk");
        while stream.advance() {
            let s = stream.token();
            println!(
                "tokens is: {:?}", s
            );
        }
        assert_eq!(true, true);
    }
}*/
