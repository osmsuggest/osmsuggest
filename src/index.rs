//#extern crate csv;
extern crate tantivy;
//#[macro_use]
//:w
//extern crate serde_derive;

use crate::config::IndexConfig;
use csv::*;
use std::path::Path;
use tantivy::directory::MmapDirectory;
use tantivy::schema::*;
use tantivy::tokenizer::*;
use tantivy::Index;

// Entrée du fichier csv
#[derive(Debug, Deserialize)]
struct Entry {
    name: String,
    alternative_names: String,
    osm_type: String,
    osm_id: u64,
    class: String,
    #[serde(rename = "type")]
    type_osm: String,
    lon: f32,
    lat: f32,
    place_rank: i8,
    importance: f32,
    street: String,
    city: String,
    county: String,
    state: String,
    country: String,
    country_code: String,
    display_name: String,
    west: f32,
    south: f32,
    east: f32,
    north: f32,
    wikidata: String,
    wikipedia: String,
    housenumbers: String,
}

pub fn prepare_index(config: &IndexConfig, source: &str) {
    let index = get_index(config);
    write_index(index, source);
}

pub fn get_index(config: &IndexConfig) -> Index {
    let path = Path::new(&config.index_path);

    if path.exists() == false {
        panic!(format!(
            "The path {} does not exists or is not readable",
            config.index_path
        ));
    }

    let directory = MmapDirectory::open(path).unwrap();

    let index: Index;

    if Index::exists(&directory) {
        index = Index::open(directory).unwrap();
    } else {
        let schema = make_schema();
        index = match Index::create(directory, schema) {
            Ok(i) => i,
            Err(e) => {
                panic!("error creating dir: {}", e);
            }
        };
    }
    add_custom_tokenizer(&index);

    index
}

fn add_custom_tokenizer(index: &Index) {
    let name_tokenizer = NgramTokenizer::new(2, 3, false).filter(LowerCaser);

    index
        .tokenizers()
        .register("name_tokenizer", name_tokenizer);
}

fn make_schema() -> Schema {
    let mut schema_builder = SchemaBuilder::default();
    let name_field_indexing = TextFieldIndexing::default()
        .set_tokenizer("name_tokenizer")
        .set_index_option(IndexRecordOption::WithFreqsAndPositions);
    let _name_field = schema_builder.add_text_field(
        "name",
        TextOptions::default()
            .set_indexing_options(name_field_indexing)
            .set_stored(),
    );
    let _display_name_field = schema_builder.add_text_field("display_name", TEXT | STORED);
    let _osm_id = schema_builder.add_u64_field("osm_id", STORED);
    let _lon_field = schema_builder.add_i64_field("lon", STORED);
    let _lat_field = schema_builder.add_i64_field("lat", STORED);

    schema_builder.build()
}

fn write_index(index: Index, source: &str) {
    //let p = Path::new("/home/julien/dev/osm//OSMNames/data/export/belgium-latest_geonames.tsv");
    let p = Path::new(source);
    let mut index_writer = index.writer(50_000_000).unwrap();
    let mut reader = ReaderBuilder::new()
        .delimiter(b'\t')
        .from_path(&p)
        .expect(&format!("Could not open file at \"{}\"", source));
    let name_field = index.schema().get_field("name").unwrap();
    let lon_field = index.schema().get_field("lon").unwrap();
    let lat_field = index.schema().get_field("lat").unwrap();
    let osm_id = index.schema().get_field("osm_id").unwrap();
    let display_name = index.schema().get_field("display_name").unwrap();

    for r in reader.deserialize() {
        let entry: Entry = r.unwrap();
        let mut document = Document::default();
        document.add_text(name_field, &entry.name);
        document.add_i64(lon_field, (entry.lon * 1_000_000_f32).trunc() as i64);
        document.add_i64(lat_field, (entry.lat * 1_000_000_f32).trunc() as i64);
        document.add_text(display_name, &entry.display_name);
        document.add_u64(osm_id, entry.osm_id);
        index_writer.add_document(document);
    }

    let commited = index_writer.commit().unwrap();
    println!("{} documents added in the store", commited);
}
