extern crate actix_web;
extern crate clap;
extern crate tantivy;
#[macro_use]
extern crate serde;
extern crate toml;

pub mod index;
pub mod server;
pub mod search;

use crate::config::*;
use clap::{AppSettings, Arg, ArgMatches, SubCommand};
use std::fs;

pub mod config {

    #[derive(Deserialize, Clone)]
    pub struct Config {
        pub index: IndexConfig,
        pub server: ServerConfig,
    }

    #[derive(Deserialize, Clone)]
    pub struct IndexConfig {
        pub index_path: String,
    }

    #[derive(Deserialize, Clone)]
    pub struct ServerConfig {
        pub bind_path: String,
        pub public_path: String
    }
}

fn serve(config: Config) {
    server::serve(config);
}

fn write_index(config: &Config, args: &ArgMatches) {
    let source = match args.value_of("source") {
        None => panic!("no csv file argument found"),
        Some(source) => source,
    };
    index::prepare_index(&config.index, source);
}

fn get_config(path: &str) -> Config {
    let data = fs::read_to_string(path).expect("Unable to open config file");
    let config: Config = toml::from_str(&data).expect("Unable to parse config file");

    config
}

fn main() {
    let matches = clap::App::new("Serve osm names")
        .version("0.1.0")
        .author("Champs Libres <info@champs-libres.coop>")
        .about("Very fast address suggest using OSM data")
        .setting(AppSettings::ArgRequiredElseHelp)
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE.toml")
                .takes_value(true)
                .default_value("./config.toml")
                .help("Configuration file using toml syntax"),
        )
        .subcommand(SubCommand::with_name("serve").about("Launch server"))
        .subcommand(
            SubCommand::with_name("index")
                .about("Index data")
                .help("Index from CSV osm name file")
                .arg(
                    Arg::with_name("source")
                        .index(1)
                        .takes_value(true)
                        .required(true),
                ),
        )
        .get_matches();

    let config_path = matches.value_of("config").unwrap();
    let config: Config = get_config(config_path);

    match matches.subcommand_name() {
        Some("serve") => {
            serve(config);
        }
        Some("index") => {
            write_index(&config, matches.subcommand_matches("index").unwrap());
        }
        _ => {
            panic!("no subcommand used. Use 'help' for full usage");
        }
    };
}
