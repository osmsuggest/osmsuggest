var horsey = require("horsey");


var getSuggestions = function(data, done) {
    var 
      searchParams = new URLSearchParams();
    searchParams.set("q", data.input);

    window.fetch("/api/search?"+searchParams.toString())
      .then(r => r.json())
      .then(function(items) {
        console.log(items);

        done(null, [{ list: items }]);
        
      });
  };

horsey(document.getElementById('box'), {
  source: getSuggestions,
  getText: 'display_name',
  getValue: 'name'
});

