console.log("hello");

window.addEventListener("load", function(e) {
  console.log(e);
  var
    list = document.getElementById("dynamic-list"),
    box  = document.getElementById("box")
     ; 
console.log(list);
console.log(box);

  box.addEventListener("input", function(e) {
    var 
      content = e.target.value,
      searchParams = new URLSearchParams();
    console.log(content);
    searchParams.set("q", content);

    window.fetch("/api/search?"+searchParams.toString())
      .then(r => r.json())
      .then(function(json) {
        console.log(json);

        for (let i=0;i < json.length; i = i+1) {
          console.log(json[i]);
          var o = document.createElement("option");
          o.textContent = json[i].name;
          list.appendChild(o);
        } 
      });
  });
});
